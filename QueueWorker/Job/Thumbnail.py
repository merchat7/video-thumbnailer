import moviepy.editor as mp
import requests
import os
import time
import Request

downloads_dir = "Downloads"
thumbnails_dir = "Thumbnails"

def download(host, bucket_name, object_name):
    url = f'http://{host}/{bucket_name}/{object_name}'
    for n in range(3):
        try:
            r = requests.get(url)
            if not os.path.exists(downloads_dir):
                os.makedirs(downloads_dir)
            with open(f"./{downloads_dir}/{object_name}", 'wb') as f:
                f.write(r.content)
            break
        except:
            if n == 2: raise
            time.sleep(10)

def upload(host, bucket_name, gif_name):
    Request.url = f"http://{host}/"
    for n in range(3):
        try:
            Request.createUploadTicket(bucket_name, gif_name)
            Request.uploadFile(bucket_name, gif_name, 1, r"./Thumbnails")
            Request.completeUploadTicket(bucket_name, gif_name)
            break
        except:
            if n == 2: raise
            time.sleep(10)

"""Download the video, make the gif, and then upload gif to bucket"""
def make(host, bucket_name, object_name):
    download(host, bucket_name, object_name)
    clip = mp.VideoFileClip(f"./{downloads_dir}/{object_name}").resize(height=240)
    clips = []
    if clip.duration < 10:
        clips.append(clip)
    else:
        parts = 5
        clip_interval = clip.duration / parts
        clip_start = 0
        for _ in range(parts):
            clip_end = clip_start + 2
            if clip_end >= clip.duration:
                break
            clips.append(clip.subclip(clip_start, clip_end))
            clip_start += clip_interval
    gif = mp.concatenate_videoclips(clips)
    if not os.path.exists(thumbnails_dir):
        os.makedirs(thumbnails_dir)
    gif_name = object_name.rsplit('.', 1)[0] + '.gif'
    gif.write_gif(f"./{thumbnails_dir}/{gif_name}")
    clip.close()
    r = upload(host, bucket_name, gif_name)
    return "Done"
