import os
import requests
import hashlib

files = [ fi for fi in os.listdir(".") if fi.endswith(".mp4") ]
buckets = ["test1", "test2"]
url = "http://127.0.0.1:5000"

def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

for bucket in buckets:
    print(f"Creating bucket: {bucket}")
    requests.post(f"{url}/{bucket}", params={'create' : ''})

count = 0
for file in files:
    with open(file, "rb") as f:
        print("Uploading:{}".format(file))
        bucketName = buckets[0]
        if count > 6:
            bucketName = buckets[1]
        fileSize = os.path.getsize(file)
        headers = {'Content-Length': str(fileSize), 'Content-MD5': md5(file)}
        requestUrl = "{}/{}/{}".format(url, bucketName, file)
        requests.post(requestUrl, params={'create': ''})
        requests.put(requestUrl, data=f, headers=headers, params={'partNumber': 1})
        requests.post(requestUrl, params={'complete': ''})
    count += 1
print("All videos uploaded")