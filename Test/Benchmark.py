import ast

import requests
import time
import os

buckets = ["test1", "test2"]
object_service_url = "http://127.0.0.1:5000"
queue_service_url = "http://127.0.0.1:7072"

def list_bucket(bucket_name):
    r = requests.get(f"{object_service_url}/{bucket_name}", params={"list": ""})
    bucket_info = ast.literal_eval(r.content.decode("utf-8"))
    if "objects" in bucket_info:
        return bucket_info["objects"]
    # Bucket is empty or does not exist
    return []

def list_bucket_filter(bucket_name, type):
    files_filtered = [file['name'] for file in list_bucket(bucket_name) if file['name'].rsplit('.', 1)[1] == type]
    return files_filtered

# Be sure to upload some videos by running Test/Videos/Upload-Videos.py first!
print("Deleting existing thumbnails if they exist")
for bucket in buckets:
    requests.delete(f"{queue_service_url}/{bucket}", params={'delete' : 'thumbnail'})
print("Creating thumbnails for all videos")
time_start = time.time()
for bucket in buckets:
    requests.post(f"{queue_service_url}/{bucket}", params={'create' : 'thumbnail'})

print("Waiting for all jobs to finish...")
while True:
    gifs_bucket = set(list_bucket_filter(buckets[0], "gif") + list_bucket_filter(buckets[1], "gif"))
    gifs_required = {video.rsplit(".", 1)[0] + ".gif" for video in os.listdir("./Videos") if video.rsplit(".", 1)[1] == "mp4"}
    if gifs_required.issubset(gifs_bucket):
        break
    time.sleep(1)
time_end = time.time()
print(f"Finished in {time_end - time_start} seconds")