import os
import sys

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

import Test.Request as Request
import QueueWorker.Job.Thumbnail
import shutil

def test_worker():
    bucket_name = "test_worker"
    object_name = "Video.mp4"
    # Initialization
    Request.deleteBucket(bucket_name) # If wasn't already deleted
    Request.createBucket(bucket_name)
    Request.createUploadTicket(bucket_name, object_name)
    Request.uploadFile(bucket_name, object_name, part=1, uploadPath=".")
    r = Request.completeUploadTicket(bucket_name, object_name)
    assert(r.status_code == 200) # Make sure all commands worked
    # Download video, make a gif, and upload gif to bucket
    QueueWorker.Job.Thumbnail.make("127.0.0.1:5000", bucket_name, object_name)
    # Check that worker actually made the gif
    gif_path = "./Thumbnails/Video.gif"
    assert(os.path.isfile(gif_path))
    # Check that worker actually downloaded the video
    download_path = "./Downloads"
    assert (os.path.isfile(f"{download_path}/{object_name}"))
    # Clean-up
    os.remove(gif_path)
    shutil.rmtree(download_path)
    r = Request.deleteBucket(bucket_name)
    assert(r.status_code == 200)

def test_service():
    # Docker / host config adjustment
    if os.path.isfile("../QueueService/Config.py.backup"):
        try:
            os.remove("../QueueService/Config.py")
        except:
            pass
    else:
        os.rename("../QueueService/Config.py", "../QueueService/Config.py.backup")
    shutil.copy("Config.py", "../QueueService/Config.py")
    assert os.path.isfile("../QueueService/Config.py")
    assert os.path.isfile("../QueueService/Config.py.backup")
    import QueueService.Helper
    # **Require .mp4 to be placed in Test/Videos and run Upload-Videos.py**
    # Test the list_videos function (which will also test list_bucket and list_bucket_filter)
    bucket1 = "test1"
    bucket2 = "test2"
    videos_info = QueueService.Helper.list_videos(bucket1)["videos"] + QueueService.Helper.list_videos(bucket2)["videos"]
    videos_test = {video_info["name"] for video_info in videos_info}
    videos_all = {video for video in os.listdir("./Videos") if video.rsplit(".", 1)[1] == "mp4"}
    # Check that all the video names are returned
    for video_name in videos_test:
        assert video_name in videos_all
    assert(len(videos_test) == len(videos_all))
    # Clean-up
    os.remove("../QueueService/Config.py")
    os.rename("../QueueService/Config.py.backup", "../QueueService/Config.py")
    assert os.path.isfile("../QueueService/Config.py")
    assert not os.path.isfile("../QueueService/Config.py.backup")
