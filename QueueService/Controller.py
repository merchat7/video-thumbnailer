from flask import Flask, request
import Verifier
import Task
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route('/<bucket_name>', methods=['POST'])
def handle_multiple_thumbnail_creation(bucket_name):
    missing_param = Verifier.check_request(request, "create", req_value="thumbnail")
    if missing_param: return missing_param
    result = Task.enqueue_all_videos_thumbnail_job(bucket_name)
    return result

@app.route('/<bucket_name>/<object_name>', methods=['POST'])
def handle_single_thumbnail_creation(bucket_name, object_name):
    missing_param = Verifier.check_request(request, "create", req_value="thumbnail", object_name=object_name, object_type="mp4")
    if missing_param: return missing_param
    result = Task.enqueue_one_video_thumbnail_job(bucket_name, object_name)
    return result

@app.route('/<bucket_name>', methods=['DELETE'])
def handle_multiple_thumbnail_deletion(bucket_name):
    missing_param = Verifier.check_request(request, "delete", req_value="thumbnail")
    if missing_param: return missing_param
    result = Task.delete_all_video_thumbnail(bucket_name)
    return result

@app.route('/<bucket_name>/<object_name>', methods=['DELETE'])
def handle_single_thumbnail_deletion(bucket_name, object_name):
    missing_param = Verifier.check_request(request, "delete", req_value="thumbnail", object_name=object_name, object_type="mp4")
    if missing_param: return missing_param
    gif_name = object_name.rsplit(".", 1)[0] + ".gif"
    result = Task.delete_one_video_thumbnail(bucket_name,gif_name)
    return result

@app.route('/<bucket_name>')
def handle_get_videos(bucket_name):
    missing_param = Verifier.check_request(request, "thumbnail")
    if missing_param: return missing_param
    result = Task.get_videos(bucket_name)
    return result

@app.route('/')
def handle_get_bucket_names():
    missing_param = Verifier.check_request(request, "list")
    if missing_param: return missing_param
    result = Task.get_buckets()
    return result

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=7072, threaded=True)