from redis import Redis
from rq import Queue
import Response as res
import Helper
import requests
import ast
from Config import redis_host, redis_port, flask_host, flask_port

conn = Redis(redis_host, redis_port)
q = Queue("Thumbnail", connection=conn)
q_failed = Queue("failed", connection=conn)
q_failed.empty() # Clear the failed queue everytime server is restarted

host = f"{flask_host}:{flask_port}"

"""Simply submit the video thumbnail job, and does not care whether it succeed or fail"""
def enqueue_one_video_thumbnail_job(bucket_name, object_name):
    try:
        # Only add the job if it's not currently queued
        # New job may be added for job that's in progress and finished job
        # but let worker handle (raise exception and add to failed queue
        # or fail gracefully aka object storage will handle)
        job_id = f"{bucket_name}/{object_name}"
        if job_id not in q.job_ids:
            q_failed.remove(job_id) # If we enqueue a job that failed, that means we are retrying and have therefore handle the error
            q.enqueue('Thumbnail.make', host, bucket_name, object_name, result_ttl=30, timeout="3h", job_id=f"{bucket_name}/{object_name}")
        # If it fail (e.g. exception raise, the job will be automatically removed from redis)
        return res.success
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

"""Do the same as enqueue_single_video_thumbnail but for the entire bucket"""
def enqueue_all_videos_thumbnail_job(bucket_name):
    try:
        # Video means .mp4 here
        videos = Helper.list_videos(bucket_name)["videos"]
        for video in videos:
            # Only making a new thumbnail task if the video doesn't already have a gif
            if not video["has_gif"]:
                result = enqueue_one_video_thumbnail_job(bucket_name, video["name"])
                if result.status_code == 400: return result
        return res.success
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

"""Simply try deleting the object from the bucket"""
def delete_one_video_thumbnail(bucket_name, object_name):
    try:
        r = requests.delete(f"http://{flask_host}:{flask_port}/{bucket_name}/{object_name}", params={'delete':''})
        response = ast.literal_eval(r.content.decode("utf-8"))
        return res.makeResponse(r.status_code, response)
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

"""Same as delete_one_video_thumbnail but for all gif in bucket"""
def delete_all_video_thumbnail(bucket_name):
    try:
        # **Will also remove other gifs that are not video thumbnail**
        for gif in Helper.list_bucket_filter(bucket_name, "gif"):
            result = delete_one_video_thumbnail(bucket_name, gif)
            if result.status_code == 400: return result
        return res.success
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

"""Not currently used"""
def get_queue_status(bucket_name):
    try:
        in_progress_jobs = []
        failed_jobs = [failed_job for failed_job in q_failed.job_ids if failed_job.split("/", 1)[0] == bucket_name]
        finished_jobs = []
        videos_name = Helper.list_bucket_filter(bucket_name, "mp4")
        for video_name in videos_name:
            if video_name not in in_progress_jobs and video_name not in failed_jobs:
                job_id = f"{bucket_name}/{video_name}"
                job = q.fetch_job(job_id)
                if job and job.result == "Done":
                    finished_jobs.append(job_id)
                elif job:
                    in_progress_jobs.append(job_id)
        return res.makeResponse(200,
                                {'status': "Success",
                                 'finished' : finished_jobs,
                                 'in_progress': in_progress_jobs,
                                 'failed': failed_jobs})
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

"""Get list of videos and whether they have a gif or not"""
def get_videos(bucket_name):
    try:
        videos = Helper.list_videos(bucket_name)
        return res.makeResponse(200,
                                videos)
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))

"""Simply call list all buckets in Object Storage (workaround CORS issue)"""
def get_buckets():
    try:
        r = requests.get(f"http://{flask_host}:{flask_port}/", params={'list': ''})
        response = ast.literal_eval(r.content.decode("utf-8"))
        return res.makeResponse(r.status_code, response)
    except Exception as e:
        return res.makeErrorResponse(400, "Error", str(e))


