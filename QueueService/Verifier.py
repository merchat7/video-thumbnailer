import Response as res

def check_request(request, command, req_value='', object_name=None, object_type=None):
    if object_name and object_name.rsplit(".", 1)[1] != object_type:
        return res.makeInvalidResponse("object type", f"Must be a .{object_type}")
    if command not in request.args:
        return res.makeMissingRequiredResponse("param", command)
    value = request.args[command].strip()
    if req_value and not value: return res.makeMissingRequiredResponse(f"value for param({command})", "<empty>")
    elif req_value != value : return res.makeInvalidResponse(f"param({command})", value)
