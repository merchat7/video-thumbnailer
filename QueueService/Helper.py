import requests
import ast
from Config import flask_host, flask_port

def list_bucket(bucket_name):
    r = requests.get(f"http://{flask_host}:{flask_port}/{bucket_name}", params={"list": ""})
    bucket_info = ast.literal_eval(r.content.decode("utf-8"))
    if "objects" in bucket_info:
        return bucket_info["objects"]
    # Bucket is empty or does not exist
    return []

def list_bucket_filter(bucket_name, type):
    files_filtered = [file['name'] for file in list_bucket(bucket_name) if file['name'].rsplit('.', 1)[1] == type]
    return files_filtered

"""List mp4 and whether they have gif"""
def list_videos(bucket_name):
    videos_map = {video_name : False for video_name in list_bucket_filter(bucket_name, "mp4")}
    for gif in list_bucket_filter(bucket_name, "gif"):
        videos_map[gif.rsplit(".", 1)[0] + ".mp4"] = True
    videos_json = {"videos" : []}
    for video_name, has_gif in videos_map.items():
        video_info = {'name': video_name, 'has_gif': has_gif}
        videos_json['videos'].append(video_info)
    return videos_json