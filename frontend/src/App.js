import React, { Component } from 'react';
import './App.css';
import VideoModal from './components/VideoModal'
import GifsContainer from './components/GifsContainer'
import { ContextMenu, MenuItem, connectMenu } from "react-contextmenu";
import axios from "axios"
import ReactTooltip from "react-tooltip";
import HelpOutlinedIcon from '@material-ui/icons/HelpOutlined';
import IconButton from '@material-ui/core/IconButton';
import Grid from "@material-ui/core/Grid";
import AsyncSelect from 'react-select/lib/Async';
import CircularProgressOverlay from "./components/CircularProgressOverlay";

const DynamicMenu = (props) => {
    const { id, trigger } = props;
    const handleItemClick = trigger ? trigger.onItemClick : null;
    return (
        <ContextMenu id={id}>
            {trigger && !trigger.hasGif && <MenuItem onClick={handleItemClick} data={{ action: 'Add' }}>{`Add thumbnail for ${"\"" + trigger.videoName + "\""}`}</MenuItem>}
            {trigger && trigger.hasGif && <MenuItem onClick={handleItemClick} data={{ action: 'Remove' }}>{`Remove thumbnail for ${"\"" + trigger.videoName + "\""}`}</MenuItem>}
            <MenuItem divider />
            {trigger && <MenuItem onClick={handleItemClick} data={{ action: 'Add all' }}>{`Add thumbnails for all videos in ${"\"" + trigger.bucketName + "\""}`}</MenuItem>}
            {trigger && <MenuItem onClick={handleItemClick} data={{ action: 'Remove all' }}>{`Remove thumbnails for all videos in ${"\"" + trigger.bucketName + "\""}`}</MenuItem>}
        </ContextMenu>
    );
};

const ConnectedMenu = connectMenu("VideoMenu")(DynamicMenu);
const controllerUrl = 'http://127.0.0.1:7072/';

class App extends Component {
    state = {
        openModal: false,
        url : "",
        fileNames: [],
        selectedOption: null,
        inputString: "",
        isLoading: false
    };

    showModal = url => {
        this.setState({
            url: url,
            openModal: true
        })
    };

    handleClose = value => {
        this.setState({openModal: false });
    };

    handleItemClick = (e, data, target) => {
        const videoName = data.videoName + ".mp4";
        switch(data.action) {
            case "Add":
                axios.post(controllerUrl + data.bucketName + "/" + videoName + "?create=thumbnail").then(
                    (res) => {alert(res.data['status'])}
                ).catch(() => {alert("Error")});
                break;
            case "Remove":
                axios.delete(controllerUrl + data.bucketName + "/" + videoName, {
                    params: {delete: "thumbnail"}
                }).then(
                    (res) => {alert(res.data['status'])}
                ).catch(() => {alert("Error")});
                break;
            case "Add all":
                axios.post(controllerUrl + data.bucketName + "?create=thumbnail").then(
                    (res) => {alert(res.data['status'])}
                ).catch(() => {alert("Error")});
                break;
            case "Remove all":
                axios.delete(controllerUrl + data.bucketName, {
                    params: {delete: "thumbnail"}
                }).then(
                    (res) => {alert(res.data['status'])}
                ).catch(() => {alert("Error")});
                break;
        }
    };

    handleInputChange = (newValue: string) => {
        const inputString = newValue;
        this.setState({inputString});
        return inputString;
    };

    handleChange = (selectedOption) => {
        let stateSelectedOption = this.state.selectedOption;
        if (!stateSelectedOption) stateSelectedOption = {};
        const bucketName = selectedOption["value"];
        let isLoading = false;
        if (bucketName !== undefined && bucketName !== stateSelectedOption["value"]) isLoading = true;
        this.setState({
            isLoading: isLoading,
            selectedOption: selectedOption,
        });
        if (bucketName === undefined) this.setState({fileNames: []});
        else {
            axios.get(controllerUrl + bucketName, {
                params: {thumbnail: ""}
            }).then((response) => {
                let fileNames = [];
                isLoading = false;
                response.data["videos"].forEach(videoName => {
                    isLoading = true;
                    let name = videoName["name"].slice(0, videoName["name"].length - 4);
                    let hasGif = videoName["has_gif"];
                    let toAppend = {bucketName: bucketName, videoName: name, hasGif: hasGif};
                    fileNames.push(toAppend)
                });
                if (fileNames.length > 0) {
                    fileNames.push({bucketName: "None", videoName: "Non-existing-Video", hasGif: false});
                }
                if (bucketName === stateSelectedOption["value"]) isLoading = false;
                this.setState({fileNames: fileNames, isLoading: isLoading});
            }).catch(() => {
                this.setState({fileNames: []})
            });
        }
    };

    filterOptions = (options) =>
        options.filter(i =>
            i.label.toLowerCase().includes(this.state.inputString.toLowerCase())
        );

    getBuckets = () => {
        return new Promise ((resolve) => {
                axios.get(controllerUrl, {
                    params: {list: ""}
                }).then(response => {
                    const datas = response.data["buckets"];
                    const toDisplay = [];
                    datas.forEach((item) => {
                        toDisplay.push({value: item, label: item});
                    });
                    resolve(this.filterOptions(toDisplay.sort((function(a, b){
                        var nameA=a.label.toLowerCase(), nameB=b.label.toLowerCase();
                        if (nameA < nameB) //sort string ascending
                            return -1;
                        if (nameA > nameB)
                            return 1;
                        return 0; //default return value (no sorting)
                    }))));
                })
            }
        );
    };

    setLoading = (value) => {
        this.setState({isLoading: value})
    };

    render() {
        const {openModal, url, fileNames, selectedOption, isLoading} = this.state;
        return (
            <div className="App">
                <Grid container
                      style={{marginTop:"50px"}}
                      direction="row"
                      justify="center"
                      alignItems="center">
                    <Grid item xs={2}>
                        <AsyncSelect
                            value={selectedOption}
                            onChange={this.handleChange.bind(this)}
                            loadOptions={this.getBuckets.bind(this)}
                            cacheOptions
                            defaultOptions
                            placeholder={"Bucket Name"}
                            onInputChange={this.handleInputChange}
                        />
                    </Grid>
                    <IconButton
                                aria-label="Help"
                                data-tip={"Start by typing a bucket name" +
                                "<br>**Video upload coming soon!**" +
                                "<br>(Gifs-showcase-v0.99.1-alpha)" +
                                "<br>[Video Controls]" +
                                "<br>Left Click - Play Video" +
                                "<br>Right Click - Video Options"}
                                data-for={"Help"}>
                        <HelpOutlinedIcon />
                    </IconButton>
                    <ReactTooltip id={"Help"} type={"info"} effect={"float"} multiline={true}/>
                </Grid>
                <GifsContainer showModal={this.showModal} fileNames={fileNames} handleItemClick={this.handleItemClick} setLoading={this.setLoading}/>
                <ConnectedMenu />
                <CircularProgressOverlay showDialog={isLoading}/>
                <VideoModal showModal={openModal} url={url} handleClose={this.handleClose}/>
            </div>
        );
    }
}

export default App;
