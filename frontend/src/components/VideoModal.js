import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import ReactPlayer from "react-player";

class VideoModal extends Component {

    render() {
        const {url, showModal, handleClose} = this.props;
        return (
            <Dialog
                open={showModal}
                onClose={handleClose}
                aria-labelledby="Video Modal"
                PaperProps={{
                    style: {
                        minWidth: "640px",
                        minHeight: "360px",
                        background: 'black',
                        overflow: 'hidden',
                    },
                }}>
                <ReactPlayer url={url} playing controls={true} />
            </Dialog>
        )
    }
}

export default VideoModal;