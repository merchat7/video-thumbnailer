import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import LinearProgress from '@material-ui/core/LinearProgress';
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";

class CircularProgressOverlay extends Component {
    render() {
        const {showDialog} = this.props;
        return (
            <Dialog
                disableBackdropClick={true}
                disableEscapeKeyDown={true}
                open={showDialog}
                aria-labelledby="Circular Progress"
                PaperProps={{
                    style: {
                        overflow: 'hidden',
                        height: "90px",
                        minWidth: "150px",
                    },
                }}>
                <DialogTitle>{"Loading..."}</DialogTitle>
                <LinearProgress />
            </Dialog>
        )
    }
}

export default CircularProgressOverlay;