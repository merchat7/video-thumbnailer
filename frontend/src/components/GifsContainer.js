import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import { ContextMenuTrigger } from "react-contextmenu";
import { withStyles } from '@material-ui/core/styles';
import ReactTooltip from "react-tooltip";

const styles = theme => ({
    gifImage: {
        height: "18vw",
        width: "32vw"
    }
});

function imagesLoaded(parentNode) {
    const imgElements = [...parentNode.querySelectorAll("img")];
    for (let i = 0; i < imgElements.length; i += 1) {
        const img = imgElements[i];
        if (!img.complete) {
            return false;
        }
    }
    return true;
}

class GifsContainer extends Component {

    handleImageChange = () => {
        this.props.setLoading(!imagesLoaded(this.galleryElement));
    };

    showGifs = () => {
        const {classes, fileNames, handleItemClick} = this.props;
        const hostName = "http://localhost:5000/";
        let gifsElement = [];
        fileNames.forEach(fileName => {
            let bucketName = fileName['bucketName'];
            let videoName = fileName['videoName'];
            let hasGif = fileName['hasGif'];
            gifsElement.push(
                <Grid item className={classes.gifImage} key={bucketName+"/"+videoName}>
                    <ContextMenuTrigger
                        id={"VideoMenu"} name={videoName}
                        holdToDisplay={1000} collect={() => {
                        return {bucketName: bucketName, videoName: videoName, hasGif: hasGif, onItemClick: handleItemClick};
                    }}>
                        <img className={classes.gifImage}
                             data-tip={videoName}
                             data-for="Video-Tooltip"
                             src={hostName + bucketName +"/" + videoName + ".gif"}
                             onClick={() => this.props.showModal(hostName + bucketName +"/" + videoName + ".mp4")}
                             alt="loading..."
                             onLoad={this.handleImageChange}
                             onError={(e) => {
                                 e.target.onerror = null;
                                 e.target.src = "https://cdn.dribbble.com/users/252114/screenshots/3840347/mong03b.gif"
                             }}/>
                    </ContextMenuTrigger>
                    <ReactTooltip scrollHide={false} id={"Video-Tooltip"} place={"top"} type={"error"} effect={"solid"}/>
                </Grid>
            )
        });
        return gifsElement
    };

    render() {
        return (
            <div className={"gallery"}
                 ref={element => {
                     this.galleryElement = element;
                 }}>
                <Grid container
                      style={{marginTop: "50px", marginLeft: "2vw", marginRight: "2vw"}}
                      direction="row"
                      justify="flex-start"
                      alignItems="flex-start">
                    {this.showGifs()}
                </Grid>
            </div>
        )
    }
}

export default  withStyles(styles)(GifsContainer);