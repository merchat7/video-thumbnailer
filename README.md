# Video Thumbnailer

Generates gifs thumbnail for video & display it on a webpage

## Getting Started

### Prerequisites

- Python 3
- Pytest
- React + NodeJS (>= 8.12.0 LTS)
- Docker + Docker-Compose
- [Object Storage](https://bitbucket.org/merchat7/object-storage)

## Deployment

```
# Clean up stopped Docker instances
docker-compose rm
# Build the Docker image
docker-compose build
# Start the Dockers instances and start two worker instances
docker-compose up --scale worker=2
```

## Running the tests

Before running the tests, be sure to put videos in "Test/Videos" and run "Upload-Videos.py"

### Correctness test

```
# Run the corectness test
cd Test
pytest
```

### Performance test

Generating .gif thumbnails for videos (~400mb, ~43 minutes) while ran in Docker for Windows takes about 3-5 minutes

```
# Run the performance test
cd Test
python Benchmark.py
```

## Issue

### File not found when trying to play video
This is a known [issue](https://github.com/docker/for-win/issues/2368) with Docker for Windows and can be fixed by disabling case sensitivity for the "buckets" folder, but this have some issues. It is therefore recommended to run the docker container on a mac/linux machine